import { Request, Response, NextFunction, Router } from "express";
import UserRouter from "./user";
const router = Router();

router.get("/health", (req, res) => {
  res.json({
    status: "success",
    status_code: 0,
    message: "API is running",
  });
});

router.use("/user", UserRouter);
export default router;
