import { Router } from "express";
import UserController from "../../controller/User.controller";

const router = Router();

router.get("/all", UserController.allUsers);
router.post("/create", UserController.createUser);
router.post("/update", UserController.updateUser);
router.post("/delete", UserController.deleteUser);

export default router;
